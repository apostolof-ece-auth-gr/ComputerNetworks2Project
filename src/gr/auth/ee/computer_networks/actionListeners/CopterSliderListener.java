package gr.auth.ee.computer_networks.actionListeners;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class CopterSliderListener implements ChangeListener {
	private final JLabel lblCopterFlightLevel;

	@SuppressWarnings("unused")
	private CopterSliderListener() {
		// Disable default constructor
		lblCopterFlightLevel = null;
	}

	public CopterSliderListener(JLabel lblCopterFlightLevel) {
		this.lblCopterFlightLevel = lblCopterFlightLevel;
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		lblCopterFlightLevel.setText("Desired flight level:\t" + ((JSlider) e.getSource()).getValue());
	}

}
