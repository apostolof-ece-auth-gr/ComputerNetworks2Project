package gr.auth.ee.computer_networks.actionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextPane;

import gr.auth.ee.computer_networks.Main;
import gr.auth.ee.computer_networks.helpers.UDPBundle;
import gr.auth.ee.computer_networks.networkTests.Image;

public class ImageListener implements ActionListener {

	private final JFormattedTextField formatedTextFieldImageRequestCode, formatedTextFieldImageDuration;
	private final JCheckBox checkBoxImageControlFlow;
	private final JComboBox<Integer> comboBoxImagePackageLength;
	private final JTextPane runtimeStatsOutput;
	private final JLabel runtimeImageOutput;
	private final UDPBundle UDPConnection;

	@SuppressWarnings("unused")
	private ImageListener() {
		// Disable default constructor
		this.formatedTextFieldImageRequestCode = null;
		this.checkBoxImageControlFlow = null;
		this.comboBoxImagePackageLength = null;
		this.formatedTextFieldImageDuration = null;
		this.runtimeStatsOutput = null;
		this.runtimeImageOutput = null;
		this.UDPConnection = null;
	}

	public ImageListener(JFormattedTextField formatedTextFieldImageRequestCode, JCheckBox checkBoxImageControlFlow,
			JComboBox<Integer> comboBoxImagePackageLength, JFormattedTextField formatedTextFieldImageDuration,
			JTextPane runtimeStatsOutput, JLabel runtimeImageOutput, UDPBundle UDPConnection) {
		this.formatedTextFieldImageRequestCode = formatedTextFieldImageRequestCode;
		this.checkBoxImageControlFlow = checkBoxImageControlFlow;
		this.comboBoxImagePackageLength = comboBoxImagePackageLength;
		this.formatedTextFieldImageDuration = formatedTextFieldImageDuration;
		this.runtimeStatsOutput = runtimeStatsOutput;
		this.runtimeImageOutput = runtimeImageOutput;
		this.UDPConnection = UDPConnection;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Main.setSubmitButtonsEnabled(false);
		Main.setStatusLineText("Test running...", Main.STATUS_LINE_ACTION_RUNNING);
		(new Image(UDPConnection, formatedTextFieldImageRequestCode.getText(),
				Integer.parseInt(formatedTextFieldImageDuration.getText()), checkBoxImageControlFlow.isSelected(),
				comboBoxImagePackageLength.getItemAt(comboBoxImagePackageLength.getSelectedIndex()), runtimeStatsOutput,
				runtimeImageOutput)).execute();
	}
}
