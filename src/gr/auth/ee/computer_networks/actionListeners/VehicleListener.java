package gr.auth.ee.computer_networks.actionListeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

import gr.auth.ee.computer_networks.Main;
import gr.auth.ee.computer_networks.helpers.TCPBundle;
import gr.auth.ee.computer_networks.helpers.UDPBundle;
import gr.auth.ee.computer_networks.networkTests.Vehicle;

public class VehicleListener implements ActionListener {
	private final JFormattedTextField formatedTextFieldVehicleRequestCode;
	private final JCheckBox checkBoxVehicleUseUDP;
	private final JFormattedTextField formatedTextFieldVehicleDuration;
	private final JLabel lblVehicleEngineRunTimeOutput;
	private final JLabel lblVehicleAirTempOutput;
	private final JLabel lblVehicleThrottlePositionOutput;
	private final JLabel lblVehicleEngineRPMOutput;
	private final JLabel lblVehicleSpeedOutput;
	private final JLabel lblVehicleCoolantTemperatureOutput;
	private final JLabel lblVehiclePacketsTotalTimeOutput;
	private final TCPBundle TCPConnection;
	private final UDPBundle UDPConnection;

	@SuppressWarnings("unused")
	private VehicleListener() {
		// Disable default constructor
		this.formatedTextFieldVehicleRequestCode = null;
		this.checkBoxVehicleUseUDP = null;
		this.formatedTextFieldVehicleDuration = null;
		this.lblVehicleEngineRunTimeOutput = null;
		this.lblVehicleAirTempOutput = null;
		this.lblVehicleThrottlePositionOutput = null;
		this.lblVehicleEngineRPMOutput = null;
		this.lblVehicleSpeedOutput = null;
		this.lblVehicleCoolantTemperatureOutput = null;
		this.lblVehiclePacketsTotalTimeOutput = null;
		this.TCPConnection = null;
		this.UDPConnection = null;
	}

	public VehicleListener(JFormattedTextField formatedTextFieldVehicleRequestCode, JCheckBox checkBoxVehicleUseUDP,
			JFormattedTextField formatedTextFieldVehicleDuration, JLabel lblVehicleEngineRunTimeOutput,
			JLabel lblVehicleAirTempOutput, JLabel lblVehicleThrottlePositionOutput, JLabel lblVehicleEngineRPMOutput,
			JLabel lblVehicleSpeedOutput, JLabel lblVehicleCoolantTemperatureOutput,
			JLabel lblVehiclePacketsTotalTimeOutput, TCPBundle TCPConnection, UDPBundle UDPConnection) {
		this.formatedTextFieldVehicleRequestCode = formatedTextFieldVehicleRequestCode;
		this.checkBoxVehicleUseUDP = checkBoxVehicleUseUDP;
		this.formatedTextFieldVehicleDuration = formatedTextFieldVehicleDuration;
		this.lblVehicleEngineRunTimeOutput = lblVehicleEngineRunTimeOutput;
		this.lblVehicleAirTempOutput = lblVehicleAirTempOutput;
		this.lblVehicleThrottlePositionOutput = lblVehicleThrottlePositionOutput;
		this.lblVehicleEngineRPMOutput = lblVehicleEngineRPMOutput;
		this.lblVehicleSpeedOutput = lblVehicleSpeedOutput;
		this.lblVehicleCoolantTemperatureOutput = lblVehicleCoolantTemperatureOutput;
		this.lblVehiclePacketsTotalTimeOutput = lblVehiclePacketsTotalTimeOutput;
		this.TCPConnection = TCPConnection;
		this.UDPConnection = UDPConnection;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Main.setSubmitButtonsEnabled(false);
		Main.setStatusLineText("Test running...", Main.STATUS_LINE_ACTION_RUNNING);
		if (checkBoxVehicleUseUDP.isSelected()) {
			new Vehicle(UDPConnection, formatedTextFieldVehicleRequestCode.getText(),
					Integer.parseInt(formatedTextFieldVehicleDuration.getText()), lblVehicleEngineRunTimeOutput,
					lblVehicleAirTempOutput, lblVehicleThrottlePositionOutput, lblVehicleEngineRPMOutput,
					lblVehicleSpeedOutput, lblVehicleCoolantTemperatureOutput, lblVehiclePacketsTotalTimeOutput)
							.execute();
		} else {
			new Vehicle(TCPConnection, formatedTextFieldVehicleRequestCode.getText(),
					Integer.parseInt(formatedTextFieldVehicleDuration.getText()), lblVehicleEngineRunTimeOutput,
					lblVehicleAirTempOutput, lblVehicleThrottlePositionOutput, lblVehicleEngineRPMOutput,
					lblVehicleSpeedOutput, lblVehicleCoolantTemperatureOutput, lblVehiclePacketsTotalTimeOutput)
							.execute();
		}
	}
}
