package gr.auth.ee.computer_networks.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class TCPBundle {
	private static final int CONNECTION_TIME_OUT = 2000;

	private String serverIp = "";
	private int serverPort = 0;
	private Socket connection = null;
	private InetAddress hostAddress = null;
	private InputStream inputStream = null;
	private OutputStream outputStream = null;

	@SuppressWarnings("unused")
	private TCPBundle() {
		// Disable default constructor
	}

	public TCPBundle(String serverIp, int serverPort) {
		this.serverIp = serverIp;
		this.serverPort = serverPort;

		try {
			String[] serverIPSplit = this.serverIp.split("\\.");
			byte[] serverIPBytes = { (byte) Integer.parseInt(serverIPSplit[0]),
					(byte) Integer.parseInt(serverIPSplit[1]), (byte) Integer.parseInt(serverIPSplit[2]),
					(byte) Integer.parseInt(serverIPSplit[3]) };
			hostAddress = InetAddress.getByAddress(serverIPBytes);

			connection = new Socket(this.hostAddress, this.serverPort);
			connection.setSoTimeout(CONNECTION_TIME_OUT);

			inputStream = connection.getInputStream();
			outputStream = connection.getOutputStream();
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	public int getServerPort() {
		return serverPort;
	}

	public Socket getConnection() {
		return connection;
	}

	public InetAddress getHostAddress() {
		return hostAddress;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public OutputStream getOutputStream() {
		return outputStream;
	}
}
